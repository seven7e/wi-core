use wip::prim::buffer::{RBuffer, WBuffer, Size, Elem};

pub struct ArrayBuffer {

}

impl RBuffer for ArrayBuffer {
    fn length(&self) -> Size {
        return 0;
    }

    fn get_elem(&self, pos: Size) -> Elem {
        return 0;
    }
}

impl WBuffer for ArrayBuffer {

    fn set_elem(&mut self, pos: Size, value: Elem) {
        return;
    }
}
